const express = require('express');
const app = express();
const morgan = require('morgan');
const path = require('path');

app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');
//middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


//ROUTES
app.get('/', (req, res) => {
   // res.sendFile('./views/index.html');
   res.sendFile(path.join(__dirname + '/views/index.html'))

});
// staring the server
app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
});